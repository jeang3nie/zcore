const std = @import("std");
const stdout = std.io.getStdOut().writer();
const stderr = std.io.getStdErr().writer();

pub fn main() anyerror!void {
    const args = try std.process.argsAlloc(std.heap.page_allocator);
    const progname = std.fs.path.basenamePosix(args[0]);

    if (args.len < 2) {
        try stderr.print("{s}: missing operand\n", .{progname});
        try usage(progname);
    } else if (args.len > 2) {
        try stderr.print("{s}: extra operands: {s}\n", .{ progname, args[2..] });
        try usage(progname);
    }
    const dir = args[1];
    const dirname = std.fs.path.dirnamePosix(dir);
    if (dirname) |val| {
        try stdout.print("{s}\n", .{val});
    }
}

fn usage(progname: []const u8) !void {
    try stderr.print("Usage: {s} path\n", .{progname});
    std.process.exit(1);
}

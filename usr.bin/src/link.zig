const std = @import("std");
const clap = @import("zig-clap");
const allocator = std.heap.page_allocator;
const process = std.process;
const stdout = std.io.getStdOut().writer();
const stderr = std.io.getStdErr().writer();

const params = comptime [_]clap.Param(clap.Help){
    clap.parseParam("-h, --help             Display this help and exit.") catch unreachable,
    clap.parseParam("-v, --verbose          Print a diagnostic for each link processed.") catch unreachable,
    clap.parseParam("<POS>...") catch unreachable,
};

pub fn main() anyerror!void {
    var diag: clap.Diagnostic = undefined;
    var args = clap.parse(clap.Help, &params, allocator, &diag) catch |err| {
        diag.report(stderr, err) catch {};
        return err;
    };
    defer args.deinit();
    if (args.flag("--help")) {
        usage(0);
    }
    switch (args.positionals().len) {
        0 => {
            try stderr.print("link: missing operand\n", .{});
            usage(1);
        },
        1 => {
            try stderr.print("link: missing operand after '{s}'\n", .{args.positionals()[0]});
            usage(1);
        },
        2 => {
            std.os.link(args.positionals()[0], args.positionals()[1], 0) catch |e| {
                try stderr.print("{s}\n", .{e});
                std.process.exit(1);
            };
            if (args.flag("--verbose")) {
                try stdout.print("link: '{s}' -> '{s}'\n", .{ args.positionals()[0], args.positionals()[1] });
            }
        },
        else => {
            try stderr.print("link: extra operand '{s}'\n", .{args.positionals()[2]});
            usage(1);
        },
    }
}

fn usage(status: u8) void {
    stderr.print("Usage: {s} ", .{"link"}) catch unreachable;
    clap.usage(stderr, &params) catch unreachable;
    stderr.print("\nFlags: \n", .{}) catch unreachable;
    clap.help(stderr, &params) catch unreachable;
    process.exit(status);
}

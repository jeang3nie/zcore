const std = @import("std");
const clap = @import("zig-clap");
const process = std.process;
const stdout = std.io.getStdOut().writer();
const stderr = std.io.getStdErr().writer();

const params = comptime [_]clap.Param(clap.Help){
    clap.parseParam("-h, --help             Display this help and exit.") catch unreachable,
    clap.parseParam("-v, --verbose          Print a diagnostic for each file processed.") catch unreachable,
    clap.parseParam("<POS>...") catch unreachable,
};

pub fn main() anyerror!void {
    var diag: clap.Diagnostic = undefined;
    var args = clap.parse(clap.Help, &params, std.heap.page_allocator, &diag) catch |err| {
        diag.report(stderr, err) catch {};
        return err;
    };
    defer args.deinit();
    if (args.flag("--help")) {
        try usage(0);
    }
    if (args.positionals().len == 0) {
        try stderr.print("unlink: missing operand\n", .{});
        try usage(1);
    }
    for (args.positionals()) |arg| {
        std.os.unlink(arg) catch |e| {
            try stderr.print("{s}\n", .{e});
            return e;
        };
        if (args.flag("--verbose")) {
            try stdout.print("unlink: {s}\n", .{arg});
        }
    }
}

fn usage(status: u8) anyerror!void {
    stderr.print("Usage: {s} ", .{"unlink"}) catch unreachable;
    clap.usage(stderr, &params) catch unreachable;
    stderr.print("\nFlags: \n", .{}) catch unreachable;
    clap.help(stderr, &params) catch unreachable;
    process.exit(status);
}

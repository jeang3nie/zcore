const std = @import("std");
const clap = @import("zig-clap");
const allocator = std.heap.page_allocator;
const base64 = std.base64;
const fmt = std.fmt;
const fs = std.fs;
const mem = std.mem;
const stdin = std.io.getStdIn();
const stdout = std.io.getStdOut().writer();
const stderr = std.io.getStdErr().writer();
const debug = std.debug;

const params = comptime par: {
    @setEvalBranchQuota(1200);
    break :par [_]clap.Param(clap.Help){
        clap.parseParam("-h, --help             Display this help and exit.") catch unreachable,
        clap.parseParam("-d, --decode           Decode rather than encode.") catch unreachable,
        clap.parseParam("-i, --ignore           Ignore whitespace when decoding.") catch unreachable,
        clap.parseParam("-w, --wrap <NUM>       Wrap encodedd lines after n characters.") catch unreachable,
        clap.parseParam("-v, --verbose          Display a header naming each file.") catch unreachable,
        clap.parseParam("-q, --quiet            Never display a header.") catch unreachable,
        clap.parseParam("<POS>...") catch unreachable,
    };
};

pub fn main() anyerror!void {
    var diag: clap.Diagnostic = undefined;
    var args = clap.parse(clap.Help, &params, allocator, &diag) catch |err| {
        diag.report(stderr, err) catch {};
        return err;
    };
    defer args.deinit();
    if (args.flag("--help")) {
        usage(0);
    }
    const wrap: u8 = if (args.option("--wrap")) |w| wblk: {
        if (fmt.parseInt(u8, w, 10)) |num| {
            break :wblk num;
        } else |e| {
            try stderr.print("{}\n", .{e});
            usage(1);
            unreachable;
        }
    } else 76;
    if (args.positionals().len > 0) {
        for (args.positionals()) |arg| {
            const contents = try get_contents(allocator, arg);
            defer allocator.free(contents);
            if (args.flag("--decode")) {
                try decode_base64(contents, args.flag("--ignore"));
            } else {
                try encode_base64(contents, wrap);
            }
        }
    } else {
        const contents = try get_contents(allocator, "-");
        defer allocator.free(contents);
        if (args.flag("--decode")) {
            try decode_base64(contents, args.flag("--ignore"));
        } else {
            try encode_base64(contents, wrap);
        }
    }
}

fn get_contents(alloc: *mem.Allocator, file: []const u8) ![]const u8 {
    const fp = if (mem.eql(u8, file, "-")) fp: {
        break :fp stdin;
    } else try fs.cwd().openFile(file, .{});
    defer fp.close();
    const contents = try fp.reader().readAllAlloc(alloc, 1000000);
    return contents;
}

fn decode_base64(contents: []const u8, ignore: bool) !void {
    var dest: [100000]u8 = undefined;
    const decoder = base64.standard.decoderWithIgnore("\n");
    _ = try decoder.decode(dest[0..], contents);
    try stdout.print("{s}", .{dest});
}

fn encode_base64(contents: []const u8, wrap: u8) !void {
    const encoder = base64.standard.Encoder;
    var dest: [100000]u8 = undefined;
    const encoded = encoder.encode(dest[0..], contents);
    for (encoded[0..]) |byte, index| {
        try stdout.writeByte(byte);
        if ((index != 0) and ((index + 1) % wrap == 0)) {
            try stdout.print("\n", .{});
        }
    }
    try stdout.print("\n", .{});
}

fn usage(status: u8) void {
    stderr.print("Usage: {s} ", .{"base64"}) catch unreachable;
    clap.usage(stderr, &params) catch unreachable;
    stderr.print("\nFlags: \n", .{}) catch unreachable;
    clap.help(stderr, &params) catch unreachable;
    std.process.exit(status);
}

const std = @import("std");
const allocator = std.heap.page_allocator;
const mem = std.mem;
const process = std.process;
const stderr = std.io.getStdErr().writer();
const stdout = std.io.getStdOut().writer();

pub fn main() anyerror!void {
    const args = try process.argsAlloc(allocator);
    defer allocator.free(args);
    while (true) {
        if (args.len > 1) {
            const string = try mem.join(allocator, " ", args[1..]);
            defer allocator.free(string);
            try stdout.print("{s}\n", .{string});
        } else {
            try stdout.print("y\n", .{});
        }
    }
}

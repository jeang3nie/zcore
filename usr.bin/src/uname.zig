const std = @import("std");
const clap = @import("zig-clap");
const allocator = std.heap.page_allocator;
const mem = std.mem;
const os = std.os;
const process = std.process;
const stdout = std.io.getStdOut().writer();
const stderr = std.io.getStdErr().writer();

const params = comptime par: {
    @setEvalBranchQuota(2000);
    break :par [_]clap.Param(clap.Help){
        clap.parseParam("-a, --all               Print all information.") catch unreachable,
        clap.parseParam("-s, --kernel-name       Print the kernel name.") catch unreachable,
        clap.parseParam("-n, --nodename          Print the network node hostname.") catch unreachable,
        clap.parseParam("-r, --release           Print the kernel release.") catch unreachable,
        clap.parseParam("-v, --version           Print the kernel version.") catch unreachable,
        clap.parseParam("-m, --machine           Print the machine hardware name.") catch unreachable,
        clap.parseParam("-p, --processor         Print the processor type.") catch unreachable,
        clap.parseParam("-i, --hardware-platform Print the harware platform.") catch unreachable,
        clap.parseParam("-o, --operating-system  Aliased to \"--kernel-name\".") catch unreachable,
        clap.parseParam("-h, --help              Display this help and exit.") catch unreachable,
    };
};

pub fn main() anyerror!void {
    var diag: clap.Diagnostic = undefined;
    var args = clap.parse(clap.Help, &params, allocator, &diag) catch |err| {
        diag.report(stderr, err) catch {};
        return err;
    };
    defer args.deinit();
    if (args.flag("--help")) {
        usage(0);
    }
    const uts = os.uname();
    const none = (!args.flag("--all") and
        !args.flag("--kernel-name") and
        !args.flag("--nodename") and
        !args.flag("--release") and
        !args.flag("--version") and
        !args.flag("--machine") and
        !args.flag("--processor") and
        !args.flag("--hardware-platform") and
        !args.flag("--operating-system"));

    var list = std.ArrayList([]const u8).init(allocator);
    defer list.deinit();

    if (args.flag("--kernel-name") or
        args.flag("--operating-system") or
        args.flag("--all") or
        none)
    {
        try list.append(uts.sysname[0..]);
    }
    if (args.flag("--nodename") or args.flag("--all")) {
        try list.append(uts.nodename[0..]);
    }
    if (args.flag("--release") or args.flag("--all")) {
        try list.append(uts.release[0..]);
    }
    if (args.flag("--version") or args.flag("--all")) {
        try list.append(uts.version[0..]);
    }
    if (args.flag("--processor")) {
        try list.append("unknown");
    }
    if (args.flag("--machine") or args.flag("--all")) {
        try list.append(uts.machine[0..]);
    }
    if (args.flag("--hardware-platform")) {
        try list.append("unknown");
    }
    const string = mem.join(allocator, " ", list.items) catch |e| {
        try stderr.print("{}\n", .{e});
        return e;
    };
    defer allocator.free(string);
    try stdout.print("{s}\n", .{string});
}

fn usage(status: u8) void {
    stderr.print("Usage: {s} ", .{"uname"}) catch unreachable;
    clap.usage(stderr, &params) catch unreachable;
    stderr.print("\nFlags: \n", .{}) catch unreachable;
    clap.help(stderr, &params) catch unreachable;
    process.exit(status);
}

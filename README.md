# Zcore
This is a toy implementation of various small Unix utilities in Zig, undertaken
as a learning exercise. Each utility is standalone. Most can be compiled by
changing into their directory and running ```zig build -Drelease-safe=true```.
They are mostly Posix compliant, but not gauranteed so.

Currently implemented:
* bin/echo
* bin/head
* bin/hostname
* bin/readlink
* bin/sync
* usr.bin/base64
* usr.bin/dirname
* usr.bin/link
* usr.bin/uname
* usr.bin/unlink
* usr.bin/yes

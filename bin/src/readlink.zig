const std = @import("std");
const clap = @import("zig-clap");
const os = std.os;
const stdout = std.io.getStdOut().writer();
const stderr = std.io.getStdErr().writer();

const params = comptime [_]clap.Param(clap.Help){
    clap.parseParam("-h, --help             Display this help and exit.") catch unreachable,
    clap.parseParam("-f, --canonicalize     Canonicalize path by recursively following every symlink in its path components.") catch unreachable,
    clap.parseParam("-n, --no-newline       Do not print the trailing newline.") catch unreachable,
    clap.parseParam("<POS>...") catch unreachable,
};

pub fn main() anyerror!void {
    var diag: clap.Diagnostic = undefined;
    var args = clap.parse(clap.Help, &params, std.heap.page_allocator, &diag) catch |err| {
        // Report useful error and exit
        diag.report(std.io.getStdErr().writer(), err) catch {};
        return err;
    };
    defer args.deinit();
    if (args.flag("--help")) {
        usage(0);
    }
    var nl = true;
    if (args.flag("--no-newline")) {
        if (args.positionals().len > 1) {
            try stdout.print("readlink: ignoring --no-newline with multiple arguments\n", .{});
        } else {
            nl = false;
        }
    }

    if (args.positionals().len > 0) {
        var buf: [4096]u8 = undefined;
        for (args.positionals()) |pos| {
            if (args.flag("--canonicalize")) {
                if (os.realpath(pos, buf[0..])) |p| {
                    try stdout.print("{s}", .{p});
                } else |e| {
                    try stderr.print("{s}\n", .{e});
                    usage(1);
                    unreachable;
                }
            } else {
                if (os.readlink(pos, &buf)) |p| {
                    try stdout.print("{s}", .{p});
                } else |e| {
                    try stderr.print("{s}\n", .{e});
                    usage(1);
                    unreachable;
                }
            }
            if (nl) {
                try stdout.print("\n", .{});
            }
        }
    }
}

fn usage(status: u8) void {
    stderr.print("Usage: {s} ", .{"readlink"}) catch unreachable;
    clap.usage(stderr, &params) catch unreachable;
    stderr.print("\nFlags: \n", .{}) catch unreachable;
    clap.help(stderr, &params) catch unreachable;
    std.process.exit(status);
}

const std = @import("std");
const clap = @import("zig-clap");
const allocator = std.heap.page_allocator;
const stdout = std.io.getStdOut().writer();
const stderr = std.io.getStdErr().writer();
const debug = std.debug;

const params = comptime [_]clap.Param(clap.Help){
    clap.parseParam("-h, --help             Display this help and exit.") catch unreachable,
    clap.parseParam("-n, --lines <NUM>      The number of lines to display.") catch unreachable,
    clap.parseParam("-c, --bytes <NUM>      Display <NUM> bytes instead of lines.") catch unreachable,
    clap.parseParam("-v, --verbose          Always print headers giving file names.") catch unreachable,
    clap.parseParam("-q, --quiet            Never print headers giving file names.") catch unreachable,
    clap.parseParam("<POS>...") catch unreachable,
};

pub fn main() anyerror!void {
    var diag: clap.Diagnostic = undefined;
    var args = clap.parse(clap.Help, &params, allocator, &diag) catch |err| {
        diag.report(std.io.getStdErr().writer(), err) catch {};
        return err;
    };
    defer args.deinit();
    if (args.flag("--help")) {
        usage(0);
    }
    var bytes = false;
    var num: u8 = 10;
    if (args.option("--bytes")) |c| {
        num = std.fmt.parseUnsigned(u8, c, 10) catch |e| {
            try stderr.print("{s}\n", .{e});
            usage(1);
            unreachable;
        };
        bytes = true;
    } else if (args.option("--lines")) |n| {
        num = std.fmt.parseUnsigned(u8, n, 10) catch |e| {
            try stderr.print("{s}\n", .{e});
            unreachable;
        };
    }
    if (args.positionals().len > 0) {
        var first = false;
        for (args.positionals()) |pos| {
            if ((args.positionals().len > 1 or args.flag("--verbose")) and !args.flag("--quiet")) {
                if (first) {
                    try stdout.print("\n==> {s} <==\n", .{pos});
                } else {
                    try stdout.print("==> {s} <==\n", .{pos});
                    first = true;
                }
            }
            try head(num, pos, bytes);
        }
    } else {
        try head(num, "-", bytes);
    }
}

fn head(count: u8, file: []const u8, bytes: bool) !void {
    if (bytes) {
        try head_bytes(count, file);
    } else {
        try head_lines(count, file);
    }
}

fn head_lines(lines: u8, file: []const u8) !void {
    var fp = if (std.mem.eql(u8, file, "-")) blk: {
        break :blk std.io.getStdIn();
    } else try std.fs.cwd().openFile(file, .{});

    defer fp.close();
    var reader = std.io.bufferedReader(fp.reader());
    var stream = reader.reader();
    var buf: [4096]u8 = undefined;
    var i: u8 = 0;
    while (try stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        if (i < lines) {
            try stdout.print("{s}\n", .{line});
            i += 1;
        } else {
            break;
        }
    }
}

fn head_bytes(bytes: u8, file: []const u8) !void {
    var fp = if (std.mem.eql(u8, file, "-")) blk: {
        break :blk std.io.getStdIn();
    } else try std.fs.cwd().openFile(file, .{});

    defer fp.close();
    var reader = std.io.bufferedReader(fp.reader());
    var stream = reader.reader();
    var i: u8 = 0;
    while (i < bytes) {
        try stdout.print("{c}", .{stream.readByte()});
        i += 1;
    }
}

fn usage(status: u8) void {
    stderr.print("Usage: {s} ", .{"head"}) catch unreachable;
    clap.usage(stderr, &params) catch unreachable;
    stderr.print("\nFlags: \n", .{}) catch unreachable;
    clap.help(stderr, &params) catch unreachable;
    std.process.exit(status);
}

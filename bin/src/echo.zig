const std = @import("std");
const allocator = std.heap.page_allocator;
const mem = std.mem;
const process = std.process;
const stdout = std.io.getStdOut().writer();

pub fn main() anyerror!void {
    const args = try process.argsAlloc(allocator);
    defer allocator.free(args);
    var start: u8 = 1;
    if (args.len > 1) {
        if (mem.eql(u8, args[1], "-n")) {
            start = 2;
        }
        var string = try std.mem.join(allocator, " ", args[start..]);
        defer allocator.free(string);
        try stdout.print("{s}", .{string});
    }
    if (start == 1) {
        try stdout.print("\n", .{});
    }
}

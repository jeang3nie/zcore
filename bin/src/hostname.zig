const std = @import("std");
const clap = @import("zig-clap");
const allocator = std.heap.page_allocator;
const mem = std.mem;
const os = std.os;
const process = std.process;
const stdout = std.io.getStdOut().writer();
const stderr = std.io.getStdErr().writer();
const sys = std.c.SYS;

const params = comptime [_]clap.Param(clap.Help){
    clap.parseParam("-h, --help   Display this help and exit.") catch unreachable,
    clap.parseParam("-s, --short  Leave off domain string.") catch unreachable,
    clap.parseParam("<name>") catch unreachable,
};

pub fn main() anyerror!void {
    var diag: clap.Diagnostic = undefined;
    var args = clap.parse(clap.Help, &params, allocator, &diag) catch |err| {
        diag.report(std.io.getStdErr().writer(), err) catch {};
        return err;
    };
    defer args.deinit();
    if (args.flag("--help")) {
        usage(0);
    }
    if (args.positionals().len < 1) {
        var buf: [64]u8 = undefined;
        const hostname = try os.gethostname(&buf);
        if (args.flag("--short")) {
            var split = mem.split(hostname, ".");
            const shortname = if (split.next()) |s| s else unreachable;
            try stdout.print("{s}\n", .{shortname});
        } else {
            try stdout.print("{s}\n", .{hostname});
        }
    } else {
        const ret = sethostname(args.positionals()[0]) catch unreachable;
        switch (os.errno(ret)) {
            0 => return,
            1 => {
                try stderr.print("hostname: sethostname: Operation not permitted\n", .{});
                process.exit(1);
            },
            else => {
                try stderr.print("hostname: sethostname: Error setting hostname\n", .{});
                process.exit(1);
            }
        }
    }
}

fn usage(status: u8) void {
    stderr.print("Usage: {s} ", .{"hostname"}) catch unreachable;
    clap.usage(stderr, &params) catch unreachable;
    stderr.print("\nFlags: \n", .{}) catch unreachable;
    clap.help(stderr, &params) catch unreachable;
    std.process.exit(status);
}

fn sethostname(name: []const u8) !usize {
    const NR_sethostname = sys.sethostname;
    const length = &name.len;
    return os.linux.syscall2(NR_sethostname, @ptrToInt(name.ptr), name.len);
}

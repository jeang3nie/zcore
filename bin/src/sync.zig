const std = @import("std");
const allocator = std.heap.page_allocator;
const stdout = std.io.getStdOut().writer();
const stderr = std.io.getStdErr().writer();

pub fn main() anyerror!void {
    const args = try std.process.argsAlloc(allocator);
    defer allocator.free(args);
    const progname = std.fs.path.basenamePosix(args[0]);

    if (args.len > 1) {
        try stderr.print("{s}: extra operand {s}\n", .{ progname, args[1..] });
        try stderr.print("Usage: {s}\n", .{progname});
        std.process.exit(1);
    }
    std.os.sync();
}

const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const programs = [_][]const u8{ "echo", "head", "hostname", "readlink", "sync" };

    for (programs) |program| {
        const source = b.fmt("src/{s}.zig", .{program});
        const exe = b.addExecutable(program, source);
        exe.setTarget(target);
        exe.setBuildMode(mode);
        exe.install();

        exe.addPackage(.{
            .name = "zig-clap",
            .path = "../lib/zig-clap/clap.zig",
        });
    }
}
